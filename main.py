import time

from PyQt5.QtWidgets import QApplication, QLabel
from PyQt5.QtGui import QPixmap, QImage

from src.RPICamera import RPICamera
from src.RPISound import RPISound
from src.LineDetection import LineDetection

if __name__ == "__main__":
    app = QApplication([])
    label = QLabel()

    rpiCamera = RPICamera(resolution=(320, 240), framerate=60)
    rpiSound = RPISound(forwardFreq=440, leftFreq=220, rightFreq=660)

    print("Program ready, CTRL-C to exit")
    T = []

    for frame in rpiCamera.frameGenerator:
        s = time.time()

        img = frame.array

        linePoints = LineDetection.getLinePoints("white", img)
        direction, img = LineDetection.getDirectionFromLine(linePoints, img)
        rpiSound.soundControl(direction)

        rpiCamera.frameContainer.truncate(0)

        image = QImage(img, img.shape[1], img.shape[0], QImage.Format_RGB888)
        label.setPixmap(QPixmap.fromImage(image))
        label.show()
        app.processEvents()

        T.append(time.time()-s)

        Tmean = sum(T)/len(T)
        print(f"Execution time:\n"
                f"Mean={Tmean*1000}ms\n"
                f"Max={max(T)*1000}ms\n"
                f"Min={min(T)*1000}ms\n")