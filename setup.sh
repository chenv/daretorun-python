# Update
sudo apt-get update

# Install PyQt5
sudo apt-get install -y qt5-default pyqt5-dev pyqt5-dev-tools

# Install Picamera
sudo apt-get install -y python-picamera python3-picamera

# Install scipy
sudo apt-get install -y python3-scipy

# Install Pyaudio
sudo apt-get install -y python3-pyaudio

# Install pip
sudo apt-get install -y python3-pip

# Install OpenCV
sudo apt-get install -y libjasper-dev libatlas-base-dev
sudo pip3 install opencv-python