import cv2 as cv
import numpy as np

class LineDetection:
    def getLinePoints(lineColor, img):
        imgGray = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
        _,imgBW = cv.threshold(imgGray, 0, 255, cv.THRESH_BINARY+cv.THRESH_OTSU)

        if lineColor == "black":
            imgBW = cv.bitwise_not(imgBW)

        lines = cv.HoughLinesP(imgBW, 1, np.pi/180, 100)

        def computeDistance(line):
            x1,y1,x2,y2 = line[0]
            return np.sqrt( (x1-x2)**2 + (y1-y2)**2 )

        try:
            D = list(map(computeDistance, lines))
        except TypeError:
            return np.array([-1, -1, -1, -1])

        maxDistance = max(D)

        longestLine = lines[D.index(maxDistance)]

        return longestLine[0]

    def getDirectionFromLine(linePoints, img):
        r1, c1, r2, c2 = linePoints

        # Get the coordinate of the center
        R, C, _ = img.shape
        rc = C//2
        cc = R//2

        try:
            # If a line is detected
            if r1 != -1:
                # Compute the coordinate of the point obtained through the orthogonal
                # projection of the center onto the line
                K = ( (rc-r1)*(r2-r1) + (cc-c1)*(c2-c1) ) / ( (r2-r1)**2 + (c2-c1)**2 )
                rp = int(K*(r2-r1) + r1)
                cp = int(K*(c2-c1) + c1)

                # Draw the line obtained through getLinePoints
                line = lambda x: int((c1 - c2)/(r1 - r2) * (x - r1) + c1)
                img = cv.line(img, (0, line(0)), (R, line(R)), (255, 0, 0), 5)
                # Draw the line that links the center and the projected point
                img = cv.line(img, (rp, cp), (rc, cc), (255, 0, 0), 5)

                # Conpute the distance between the projected point and the center
                d = np.sqrt((rp-rc)**2+(cp-cc)**2)

            # [0; delta] represents the interval in which we consider to be on the line
            delta = 30

            # If cp == NaN (when the getLinePoints found no line)
            if r1 == -1:
                # no direction
                direction = np.int8(0)
            # Elif d is inside [0; delta]
            elif d < delta:
                # Forward
                direction = np.int8(2)
            # Elif d is outside [0; delta] and we are on the left side of the line
            elif cp < cc:
                # Left
                direction = np.int8(-1)
            # Elif d is outside [0; delta] and we are on the right side of the line
            elif cp > cc:
                # Right
                direction = np.int8(1)
            # Else (a case we didn't handle)
            else:
                direction = np.int8(0)
        except Exception:
            direction = np.int8(0)

        return direction, img