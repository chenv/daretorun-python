import time
from scipy import signal
import numpy as np
import pyaudio

class RPISound:
    def __init__(self, forwardFreq, leftFreq, rightFreq):
        self.player = pyaudio.PyAudio()
        self.fs = 44100
        self.stream = self.player.open(format=pyaudio.paFloat32,
                        channels=2,
                        rate=self.fs,
                        output=True)

        self.forwardSound = self.generateSquareSound(forwardFreq)
        self.leftSound = self.generateSineSound(leftFreq)
        self.rightSound = self.generateSineSound(rightFreq)


    def generateSineSound(self, f, duration=0.1):
        # generate samples, note conversion to float32 array
        soundArray = (np.sin(2 * np.pi * np.arange(self.fs * duration) * f / self.fs)).astype(np.float32)
        return soundArray

    def generateSquareSound(self, f, duration=0.1):
        # generate samples, note conversion to float32 array
        soundArray = signal.square(2 * np.pi * np.arange(self.fs * duration) * f / self.fs).astype(np.float32)
        return soundArray

    def playSound(self, soundArray):
        outputBytes = (soundArray).tobytes()
        self.stream.write(outputBytes)

    def soundControl(self, direction):
        # Generate stereo sound according to the direction
        # INPUT:
        #     forwardSound: sound to be played if the direction is forward
        #     leftSound: sound to be played if the direction is left
        #     rightSOund: sound to be played if the direction is right
        #     direction: integer that represents a direction:
        #         - int(0): no direction found (usually no line found)
        #         - int(2): forward
        #         - int(-1): left
        #         - int(+1): right
        # OUTPUT:
        #     outputSound: sound array which will guide the user

        # If direction is forward
        if direction == 2:
            # Copy the forwardSound to the left and right channel
            outputSound = np.vstack((0.25*self.forwardSound, 0.25*self.forwardSound)).T

        # Else
        else:
            # If direction is right
            if direction == 1:
                # Enable right channel and disable left channel
                leftAmplitude = 0
                rightAmplitude = 1

            # If direction is left
            elif direction == -1:
                # Enable left channel and disable right channel
                leftAmplitude = 1
                rightAmplitude = 0

            # Else no direction
            else:
                # DIsable right and left channels
                leftAmplitude = 0
                rightAmplitude = 0

            # Generate output sound based on previous condition
            outputSound = np.vstack((leftAmplitude * self.leftSound, rightAmplitude * self.rightSound)).T

        self.playSound(0.5*outputSound)

    def stop(self):
        self.stream.stop_stream()
        self.stream.close()
        self.player.terminate()