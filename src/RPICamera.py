import time
import picamera
import picamera.array
import numpy as np
from PIL import Image

class RPICamera:
    def __init__(self, resolution, framerate):
        # Create PiCamera object
        self.camera = picamera.PiCamera()
        self.camera.resolution = resolution
        self.camera.framerate = framerate

        # Start camera
        self.camera.start_preview()
        time.sleep(2)
        print("Camera ready")

        # Initialize frame container and generator
        self.frameContainer = picamera.array.PiRGBArray(self.camera)
        self.frameGenerator = self.camera.capture_continuous(self.frameContainer, format="rgb")

    def stop(self):
        self.camera.stop_preview()
        self.camera.close()
        print("Camera stopped")