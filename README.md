<div align=center>

<img src="./assets/daretorun.png" align="center" width="250" alt="DareToRun icon">

# DareToRun-Python
*Assistive device for visually impaired people using **Raspberry Pi***

---
## Software and hardware 

<img src="./assets/raspberry-pi.png" align=center height=50px width=auto> ***Raspberry Pi 4 Model B***

<img src="./assets/python.png" align=center height=50px width=auto> ***Python***

---
## Academic project page
[***s5project***](https://gitlab.eurecom.fr/renaud.pacalet/s5project)

---
## Project website
[***Website***](https://maleksfaxi.github.io/DareToRun/)

---
## Original git project page
[***DareToRun***](https://gitlab.eurecom.fr/chieu/daretorun)

---
## Supervisors
[Renaud Pacalet]

[Chiara Galdi]

[Massimiliano Todisco]

[Melek Önen] (Tutor)

---
## Team members
[Villon Chen]

[William Chieu]

[Madleen Jean]

[Elena Lassalle]

[Malek Sfaxi]

</div>

---
## Overview
The objective of the project S5 is to create a device allowing visually impared people to follow a line. 

The device we came up with is composed of a Rasberry Pi connected to earphones which gives audio output and to a camera which can detect the line.

---
## Guide

### Clone the repository
```
git clone https://gitlab.eurecom.fr/chenv/daretorun-python.git
cd daretorun-python
```

### Expand filesystem
By default, the storage needs to be expand to increase the storage.
```bash
sudo raspi-config --expand-rootfs
```

### Install required packages and libraries
```bash
sudo ./setup.sh
```
The installation can be long (10min+) as some dependencies are built by the Raspberry Pi (mainly for OpenCV).

---
## Run
```bash
python3 main.py
```

---
## Project structure
```
.
├── assets                      # Static files
|
├── src                         # Source code
|   ├── RPICamera.py            # Raspberry Pi camera
|   ├── RPISound.py             # Raspberry Pi sound player
|   └── LineDetection.py        # Line detection tools
|
├── main.py                     # Main script
├── setup.sh                    # Install dependencies script
├── README.md
|
└── .gitignore
```
<!--- Contacts -->
[Renaud Pacalet]: mailto:Renaud.Pacalet@telecom-paris.fr
[Chiara Galdi]: mailto:Chiara.Galdi@eurecom.fr
[Massimiliano Todisco]: mailto:Massimiliano.Todisco@eurecom.fr
[Melek Önen]: mailto:Melek.Onen@eurecom.fr

[Villon Chen]: mailto:Villon.Chen@eurecom.fr
[William Chieu]: mailto:William.Chieu@eurecom.fr
[Madleen Jean]: mailto:Madleen.Jean@eurecom.fr
[Elena Lassalle]: mailto:Elena.Lassalle@eurecom.fr
[Malek Sfaxi]: mailto:Malek.Sfaxi@eurecom.fr
